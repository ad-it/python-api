# python-api

## Description

A python script which interacts with https://dictionaryapi.com API and displays information about a word.

Usage:
```
python3 dict.py *word* *your_api_key*
```
Output example:
```
ex*am*ple : (noun) one that serves as a pattern to be imitated or not to be imitated

```
Result will be displayed in a console and saved in a file named artifact.txt.