import requests
import sys

word = sys.argv[1]
api_Key = sys.argv[2]
url = "https://dictionaryapi.com/api/v3/references/collegiate/json/{}?key={}".format(word, api_Key)

response = requests.get(url)
json_response = response.json()

hwi = json_response[0]['hwi']['hw']
func_label = json_response[0]['fl']
short_def=json_response[0]['shortdef']

result=hwi+' : '+'('+func_label+') '+short_def[0]
print(result)

with open('artifact.txt', 'w') as f:
  sys.stdout = f
  print(result)
